const { src, dest, parallel } = require('gulp');
const minifyCSS = require('gulp-csso');
const concat = require('gulp-concat');
const gutil = require('gulp-util');
const htmlmin = require('gulp-htmlmin');
const purgecss = require('gulp-purgecss')

function html() {
    return src('*.html')
        .pipe(htmlmin({ collapseWhitespace: true, minifyJS: true, removeComments: true, useShortDoctype: true, includeAutoGeneratedTags: false }))
        .pipe(dest('public'))
}

//'public/css/*.css'
function css() {
    return src([
        'app.css'
    ])
        .pipe(minifyCSS())
        .pipe(concat('app.min.css'))
        .pipe(purgecss({
            content: ['index.html']
        }))
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(dest('public/css'))
}

exports.css = css;
exports.html = html;

// only do css and html
exports.default = parallel(css, html);